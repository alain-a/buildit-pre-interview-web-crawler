
How to run:
- build the Maven project
- run the WebCrawler class

Assumptions:
- neither authentication nor authorization to deal with.
- we only process static content i.e. links within HTML responses. We do not consider possible links within potential
  AJAX calls to REST if they are not rendered as HTML and cannot be navigated to by following hyperlinks. If we wanted to
  also process those, we would need to use an engine able to interpret JavaScript
- it is OK to use a 3rd-party HTML parser, for speed of development and avoiding unforeseen bugs and edge cases
  we would have to handle manually. JSoup chosen because most recently updated. Also, available from Maven repo so easy
  to integrate into a build (minor advantage). Check License.
  https://en.wikipedia.org/wiki/Comparison_of_HTML_parsers
- meta elements like <meta content="http://www.google.com/logos/doodles/2016/juno-reaches-jupiter-5164229872058368.4-thp.png"
  property="og:image"> are not processed (but could easily be)
- the restricted list of content types acceptable to follow a link is: text/*, application/xml, or application/xhtml+xml
  (this could be expanded playing with the JSoup API, by this is the default to be able to parse a link)
- any issue while parsing a followed URL is logged but prevented from stopping the crawling (as in reading
  http://www.wipro.com/documents/resource-center/Content_Monetization_Strategies_for_the_Digital_Publisher.pdf which MIME type
  is not supported by default)
- For speed, we could try and avoid navigating to unsupported content type such as email addresses

Issues, bugs, todos etc:
- occasionally times out on socket read (attempting to fetch a page). Hacky solution: increased the timeout.
- add more unit tests
- replace System.out.println() in API implementation with logging framework
- consider abstracting API as interfaces
- consider perf improvement using parallelism over cores
- consider replacing recursion with loop to avoid running into stack explosion issues)
