package aa.buildit.webcrawler;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

public class PageGraph {

    static String getDomainName(String url) throws Exception {
        // See http://goo.gl/QzkgS9
        if(!url.startsWith("http") && !url.startsWith("https")){
            url = "http://" + url;
        }
        final URI uri = new URI(url);
        final String host = uri.getHost();
        final String prefix = "www.";
        return host.startsWith(prefix) ? host.substring(prefix.length()) : host;
    }

    static GraphNode get(String url) throws Exception {
        final String domain = getDomainName(url);
        final Set<String> followedUrls= new HashSet<>();
        final GraphNode rootPage = GraphNode.getPageNode(url, domain, followedUrls);
        rootPage.buildGraph();
        return rootPage;
    }
}
