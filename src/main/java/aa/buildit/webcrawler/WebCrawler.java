package aa.buildit.webcrawler;

import java.util.List;

public class WebCrawler {



    private void printPageGraph(GraphNode graphNode) {

        System.out.println("--------------- BEGIN " + graphNode.getUrl() + "--------------");

        printPageUrl(graphNode.getUrl());

        for (String importUrl : graphNode.getImports()) {
            printImport(importUrl);
        }

        for (String mediaUrl : graphNode.getMedia()) {
            printMedia(mediaUrl);
        }

        for (String externalPageUrl : graphNode.getExternalLinks()) {
            printExternalPageUrl(externalPageUrl);
        }

        for (String internalPageUrl : graphNode.getInternalLinks()) {
            printExternalPageUrl(internalPageUrl);
        }

        // Followed links (assuming no cycles in the graph)
        final List<GraphNode> followedChildUrls = graphNode.getFollowedChildUrls();
        if (!followedChildUrls.isEmpty()) {
            System.out.println(" * Followed links (if not already reported) with their details:");
            for (GraphNode externalPage : followedChildUrls) {
                // TODO identation
                printPageGraph(externalPage);
            }
        }

        // no need for polymorphism on link type here, let's not go crazy for now.

        System.out.println("--------------- END " + graphNode.getUrl() + "--------------");
    }

    private void printImport(final String importUrl) {
        print(" * import: <%s>", importUrl);
    }

    private void printMedia(final String mediaUrl) {
        print(" * src: <%s>", mediaUrl);
    }

    private void printPageUrl(final String pageUrl) {
        print(" * URL: <%s>", pageUrl);
    }

    private void printExternalPageUrl(final String pageUrl) {
        print(" * a: <%s>", pageUrl);
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    public static void main(String args[]) throws Exception {
        //final String url = "monest.co.uk"; //args[0];
        final String url = "wipro.com";
        //final String url = "http://www.wipro.com/documents/resource-center/Content_Monetization_Strategies_for_the_Digital_Publisher.pdf";
        final GraphNode rootPage = PageGraph.get(url);
        new WebCrawler().printPageGraph(rootPage);
    }
}
