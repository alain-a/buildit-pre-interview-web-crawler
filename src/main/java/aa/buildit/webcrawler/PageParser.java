package aa.buildit.webcrawler;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

class PageParser {

    private static final int CONNECTION_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(10);
    private final Document document;

    PageParser(String url) throws Exception {
        if(!url.startsWith("http") && !url.startsWith("https")){
            url = "http://" + url;
        }
        document = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT).get();
    }

    List<String> getImports() throws Exception {
        Elements imports = document.select("link[href]");
        final List<String> links = imports.stream().map(src -> src.attr("abs:href")).collect(Collectors.toList());
        // TODO potentially add descriptive info like title
        return links;
    }

    List<String> getMedia() throws Exception {
        Elements media = document.select("[src]");
        final List<String> links = media.stream().map(src -> src.attr("abs:src")).collect(Collectors.toList());
        // TODO potentially add descriptive info like title
        return links;
    }

    List<String> getLinks() throws Exception {
        Elements anchorElements = document.select("a[href]");
        final List<String> links = anchorElements.stream().map(e -> e.attr("abs:href")).collect(Collectors.toList());
        // TODO potentially add descriptive info like title
        return links;
    }

}
