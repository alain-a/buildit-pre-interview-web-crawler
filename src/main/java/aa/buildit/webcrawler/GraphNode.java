package aa.buildit.webcrawler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class GraphNode {
    private final String url;
    private final String domain;

    private List<String> media;
    private List<String> imports;
    private final List<String> internalLinks;
    private final List<String> externalLinks;

    private final List<GraphNode> followedChildUrls;

    private final Set<String> allFollowedUrlsInGraph;

    private GraphNode(
        final String domain, final String url,
        final List<String> media, final List<String> imports,
        final List<String> internalLinks, final List<String> externalLinks,
        final Set<String> allFollowedUrlsInGraph) {
        this.domain = domain;
        this.url = url;
        this.media = media;
        this.imports = imports;
        this.internalLinks = internalLinks;
        this.externalLinks = externalLinks;
        this.allFollowedUrlsInGraph = allFollowedUrlsInGraph;
        this.followedChildUrls = new ArrayList<>();
    }

    String getUrl() {
        return url;
    }

    List<String> getInternalLinks() {
        return internalLinks;
    }

    List<String> getExternalLinks() {
        return externalLinks;
    }

    List<String> getMedia() {
        return media;
    }

    List<String> getImports() {
        return imports;
    }

    List<GraphNode> getFollowedChildUrls() {
        return this.followedChildUrls;
    }

    void buildGraph() {
        final List<String> internalLinks = this.getInternalLinks();
        for(String childUrl : internalLinks) {
            if (!allFollowedUrlsInGraph.contains(childUrl)) {
                final Optional<GraphNode> maybeChildNode = followAndParseUrl(childUrl);
                allFollowedUrlsInGraph.add(url);

                if (maybeChildNode.isPresent()) {
                    final GraphNode childNode = maybeChildNode.get();
                    this.followedChildUrls.add(childNode);
                    childNode.buildGraph(); // recursively build the graph
                }
            }
        }
    }

    private Optional<GraphNode> followAndParseUrl(final String childUrl) {
        GraphNode childNode = null;
        try {
            childNode = getPageNode(childUrl, domain, allFollowedUrlsInGraph);
        } catch (Exception e) {
            logWarning("WARNING: An error happened while parsing the url:" + url + "\n" + e.getMessage());
        }
        return Optional.ofNullable(childNode);
    }

    static GraphNode getPageNode(
            final String url, final String domain, final Set<String> allFollowedUrlsInGraph) throws Exception {

        logDebug("About to parse " + url);

        final PageParser page = new PageParser(url);

        final List<String> media = page.getMedia();
        final List<String> imports = page.getImports();

        final List<String> links = page.getLinks();
        final List<String> internalLinks = new ArrayList<>();
        final List<String> externalLinks = new ArrayList<>();
        for (String link : links) {
            if (link.contains(domain)) {
                internalLinks.add(link);
            }
            else {
                externalLinks.add(link);
            }
        }
        return new GraphNode(domain, url, media, imports, internalLinks, externalLinks, allFollowedUrlsInGraph);
    }

    private static void logWarning(final String s) {
        System.err.println(s);
        // TODO replace with logging API
    }

    private static void logDebug(final String s) {
        System.out.println(s);
        // TODO replace with logging API
    }
}
