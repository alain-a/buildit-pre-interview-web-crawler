package aa.buildit.webcrawler;

import java.util.List;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;

public class PageParserTest {

    // TODO find a stable URL to test against (currently this test is brittle).
    // Better: make and use a canned HTML response for testing purposes

    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void shouldReturnAllLinksInPage() throws Exception {
        final List<String> links = new PageParser("wipro.com").getLinks();
        softly.assertThat(links.size()).isEqualTo(147);

        // TODO test where we compare actual links to expected
    }

    @Test
    public void shouldReturnAllMediaInPage() throws Exception {
        final List<String> links = new PageParser("wipro.com").getMedia();
        softly.assertThat(links.size()).isEqualTo(24);

        // TODO test where we compare actual links to expected
    }

    @Test
    public void shouldReturnAllImportsInPage() throws Exception {
        final List<String> links = new PageParser("wipro.com").getImports();
        softly.assertThat(links.size()).isEqualTo(3);

        // TODO test where we compare actual links to expected
    }

}
