package aa.buildit.webcrawler;

import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;

public class PageGraphTest {

    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void shouldExtractDomain() throws Exception {
        // Given
        final String url = "wiprodigital.com";

        // When
        final String domainName = PageGraph.getDomainName(url);

        // Then
        softly.assertThat(domainName).isEqualTo("wiprodigital.com");
    }

    // TODO add test for getting the graph

}
